#!/bin/bash

IMG="$1"
DEV="$2"

if [[ -z "$(command -v growpart)" || -z "$(command -v btrfs)" ]]; then
	echo "growpart oder btrfs fehlt! Installiere cloud-guest-utils und btrfs-progs..."
	sudo apt update
	sudo apt install -y cloud-guest-utils btrfs-progs
fi

if [ -z "$IMG" ] ; then
echo "Image-File angeben! *.img *.bz2 Param #1 Param #2 = Gerät "
exit 1
fi

if [ -z "$DEV" ] ; then
echo "HDD dev angeben! z. B. /dev/hdc  Param #2"
exit 1
fi

echo "Warte 5 Sekunden vor dem löschen von ${DEV}, abbrechen mit strg + c..."
sleep 5
echo "Achtung - vorbei, ich mache jetzt alles platt auf" "${DEV}"

case "$IMG" in
  *.bz2)
    # alternativ: `bzcat` statt `bzip2 -dc`	  
    bzip2 -dc "$IMG" | sudo dd of="$DEV" bs=1M status=progress
    ;;
  *)
    sudo dd if="$IMG" of="$DEV" bs=1M status=progress
    ;;
esac
sync
sudo growpart $DEV 3
sudo mount  ${DEV}3 /mnt
sudo btrfs filesystem resize max /mnt
sudo umount /mnt
sync
