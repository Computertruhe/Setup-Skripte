#!/bin/bash

if [ -z "$(command -v dialog)" ]; then
        echo "dialog fehlt! Installiere dialog..."
        sudo apt update
        sudo apt install -y dialog btrfs-progs
fi


GG=$(lsblk -r -p -n -o NAME,TYPE |grep disk | cut -f1 -d' ') 
echo $GG
Devs=()
for dev in $GG; do
    Devs+=($dev)
    Devs+=("")
done


while true
do

  dev=`dialog --menu "Platte einlesen. Festplattengerät wählen" 0 0 0 \
    "${Devs[@]}" 3>&1 1>&2 2>&3`
  dialog --clear
  dialog --yesno "Bestätigen Sie Ihre Auswahl: $dev" 0 0

# Get exit status
# 0 means user hit [yes] button.
# 1 means user hit [no] button.
# 255 means user hit [Esc] key.
response=$?
case $response in
   0) echo "Bestätigt"; break;;
   1) echo "Neu wählen";;
   255) echo "[ESC]"; exit 1;;
esac

done




while true
do
  dialog --clear
  # show an inputbox
  FILENM=$(dialog --title "Imagedatei" --inputbox "Dateiname \*.img " 8 60  3>&1 1>&2 2>&3 3>&- )

  # get respose
  respose=$?
  case $response in
   1) exit 2 ;;
   255) echo "[ESC]"; exit 1;;
  esac


  dialog --clear
  dialog --yesno "Gewählte Datei: $FILENM" 0 0
  # Get exit status
  # 0 means user hit [yes] button.
  # 1 means user hit [no] button.
  # 255 means user hit [Esc] key. 
  response=$?
  case $response in
   0) echo "Bestätigt"; break;;
   1) echo "Neu wählen";;
   255) echo "[ESC]"; exit 1;;
  esac


done

echo "Ermittle letzten Sektor von Quelldevice $dev"
LASTSECT=$(sudo fdisk -x "$dev" | tail -1 |  awk '{print $3}' )

  dialog --clear
  dialog --yesno "Letzter Sektor von letzter Partition auf $dev ist $LASTSECT . OK?" 0 0
  # Get exit status
  # 0 means user hit [yes] button.
  # 1 means user hit [no] button.
  # 255 means user hit [Esc] key. 
  response=$?
  case $response in
   0) echo "Bestätigt";;
   1) echo "Sicherheitshalber beendet"; exit 1;;
   255) echo "[ESC]"; exit 1;;
  esac



echo "Bereit $dev bis $LASTSECT in $FILENM zu schreiben"




sudo dd if=$dev of="$FILENM" bs=512 count=$LASTSECT status=progress
