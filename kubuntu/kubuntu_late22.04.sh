#!/usr/bin/env bash

# execute self with sudo if user is not root
[[ $UID -ne 0 ]] && exec sudo ./"$0"

###
# Set variables
###
export DEBIAN_FRONTEND=noninteractive
DPKG_CFG_FRAGMENT=/etc/dpkg/dpkg.cfg.d/non-interactive

# Linux distro
script_distro='Kubuntu Linux 22.04'

# Additional packages
packages=(
  libreoffice
  keepassxc
  kpartx
#  oem-config
  hardinfo
  regionset
  cheese
  vlc

  # Language support
  language-selector-common
  openoffice.org-hyphenation
  
  # AR
  language-pack-kde-ar
  fonts-arabeyes
  hunspell-ar
  libreoffice-l10n-ar
  mythes-ar
  hunspell-ar
  thunderbird-locale-ar

  # DE
  language-pack-kde-de # Installed only with Internet connection during the OEM setup.
  firefox-locale-de
  thunderbird-locale-de # Installed only with Internet connection during the OEM setup.
  hunspell-de-de
  mythes-de
  hyphen-de
  mythes-de
   
  # EN
  thunderbird-locale-en-gb
  
  # FA
  language-pack-kde-fa
  firefox-locale-fa
  thunderbird-locale-fa
  libreoffice-l10n-fa
  myspell-fa
    
  # RU
  language-pack-kde-ru
  firefox-locale-ru
  thunderbird-locale-ru
  hyphen-ru
  mythes-ru
  hunspell-ru

  # UK
  language-pack-kde-uk
  libreoffice-l10n-uk
  firefox-locale-uk
  thunderbird-locale-uk
  hyphen-uk
  mythes-uk
  hunspell-uk
  wukrainian
)

###
# Functions
###

# $*: message to echo
e() {
  printf "\e[31m>>>\e[0m %s\n" "$*"
}

# It's a pain to stop (ctrl+c, kill) execution with all the Python applications
# so we catch SIGINT and SIGTERM and exit immediately
handle_signal() {
  e "Ausführung abgebrochen! Skript wird beendet."
  exit 1
}

# always executed on shell exit regardless of reason. for cleanup tasks
handle_exit() {
  e "Finales Cleanup wird durchgeführt …"
  rm -f "$DPKG_CFG_FRAGMENT"
  rm -f /etc/apt/apt.conf.d/01aptproxy
}

trap handle_signal SIGINT SIGTERM
trap handle_exit EXIT

pkg() {
  apt install "$@" --yes --quiet
}

###
# Greeting
###
echo "\
#####################################
# Computertruhe-Installationsskript #
#####################################

###
# Maintainer: Computertruhe e. V.
# Website:    https://computertruhe.de/
# Version:    1.0.1
# Repo:       https://codeberg.org/Computertruhe/computertruhe-setup
# Distro:     ${script_distro}
###
"
e "Starte initiales Setup für Rechner mit frisch installiertem '${script_distro}' …"

# Generell auto-apt-proxy installieren
# tut nicht weh und erweitert apt jetzt und in Zukunft, einen host apt-proxy oder andere
# z.b. in SRV-Records benannte caches zu verwenden. gut für spätere updates.
e "Apt proxy automatisch verwenden"
apt install auto-apt-proxy 

###
# Automatic installation
###
# place temporary dpkg configurations to ensure non-interactive upgrade
cat <<'DPKG' >"$DPKG_CFG_FRAGMENT"
force-confold
force-confdef
DPKG

e "Paketquellen aktualisieren …"
apt update --yes --quiet

e "System aktualisieren (apt) …"
apt full-upgrade --yes --quiet

e "Proprietäre Treiber installieren (sofern verfügbar) …"
ubuntu-drivers autoinstall

e "Zusätzliche Software installieren …"
pkg "${packages[@]}"

e "Sprachunterstützung vervollständigen …"
pkg $(check-language-support)
e "Arabische Schrift"
pkg fonts-arabeyes
e "Kyrillische Schrift"
pkg xfonts-cyrillic

# these packages have to be installed after "$(check-language-support)"
pkg hunspell-de-at hunspell-de-ch hunspell-de-de hunspell-uk hunspell-ru

e "Unnötige Pakete entfernen und Cache bereinigen …"
apt clean --yes --quiet
apt autoremove --yes --quiet


###
# Finalisation
###

e "Initiales Setup beendet."
oem-config-prepare
