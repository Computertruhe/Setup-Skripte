#!/bin/bash

# skript baut ein subvol /@swap unten drunter und mountet das
# ganze als /swap um darin /swap/swapfile als swapspace
# zu aktivieren - größe 8G oder was man sich ausdenkt

# It is possible to use a swap file on btrfs, but there are some considerations that need taking care of.
# btrfs filesystem doesn't let to create snapshots if there is a working swap file on the subvolume. That means that it is highly recommended to place a swap file on a separate subvolume.
# Let's assume that the current swap is already off, the / is on /dev/sda1 and Ubuntu is installed with / on @ subvolume and /home is on @home subvolume.
#  Mount /dev/sda1 to /mnt.

rootdevsrc=$(findmnt -nr -T / -osource)
echo "Mount / ist gebunden auf $rootdevsrc"
if [[ "$rootdevsrc" =~ \[/@]$ ]] 
then
   echo "Verwendung von btrfs subvol /@"
else
   echo "Hier stimmt was nicht. $rootdevsrc enthält kein btrfs-subvol oder subvol ist nicht '/@'"
   exit 1
fi

rootdev=${rootdevsrc%\[/@]}

echo "BTRFS-Device ist daher $rootdev"
echo "OK - erzeuge jetzt (etwas umständlich) ein SubVol /@swap, in dem swapfile liegt"

# zunächst nochmal mounten, damit die root-subvols sichtbar werden
sudo mount $rootdev /mnt
#    If you run ls /mnt, you'll see @, @home and other subvolumes that may be there.

#   Create a new @swap subvolume.
sudo btrfs sub create /mnt/@swap

#    Unmount /dev/sda1 from /mnt.
sudo umount /mnt

#    Create /swap directory where we plan to mount the @swap subvolume.
sudo mkdir /swap

#    Mount the @swap subvolume to /swap.
sudo mount -o subvol=@swap $rootdev /swap

#    Create the swap file.
sudo touch /swap/swapfile

#    Set 600 permissions to the file.
sudo chmod 600 /swap/swapfile

#    Disable COW for this file.
sudo chattr +C /swap/swapfile

#    Set size of the swap file to 8G as an example.
sudo dd if=/dev/zero of=/swap/swapfile bs=1M count=8192

#    Format the swapfile.
sudo mkswap /swap/swapfile

#    Turn the swap file on.
sudo swapon /swap/swapfile

# Now the new swap should be working.
#
# You also need to update /etc/fstab to mount all this on boot. Add there two lines:
echo "Update /etc/fstab mit uuid Spezifizierung"
# uuid from Dev
uuid=$(lsblk -n -o UUID $rootdev)
sudo echo "UUID=$uuid /swap btrfs  defaults,subvol=@swap 0 0" >> /etc/fstab
sudo echo "/swap/swapfile none swap sw 0 0" >> /etc/fstab

# The UUID is the one of your /dev/sda1.
echo "Fertig! Swap ist nun im Subvol @swap"
sudo swapon